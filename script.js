function addPeople() {
    var name = $('#txtname').val();
    var lastname = $('#txtlastname').val();
    if (name.length > 0 && lastname.length > 0) {
        $.post(
            'post.php',
            {
                name: name,
                lastname: lastname
            }, (data)=>{
                data = JSON.parse(data);
                addRow(data);
            }
        );
    }
    $('#txtname').val('');
    $('#txtlastname').val('');
}

function addRow(data) {
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    var td1 = document.createElement('td');
    var td2 = document.createElement('td');
    var td3 = document.createElement('td');
    var td4 = document.createElement('td');
    var input = document.createElement('input');
    var people = document.getElementById('people');

    tr.className = 'border-bottom';
    td.innerText = data.id;
    tr.appendChild(td);
    td1.innerText = data.name;
    tr.appendChild(td1);
    td2.innerText = data.lastname;
    tr.appendChild(td2);
    td3.innerText = data.date;
    tr.appendChild(td3);

    input.type = 'checkbox';
    input.checked = data.attendance;
    input.onclick = (e) => {
        $.post(
            'post.php',
            {
                id: data.id,
                attendance: e.target.checked
            }
        )
    }

    td4.appendChild(input);
    tr.appendChild(td4);

    people.appendChild(tr);
}

function exportPeople(){
    $.get(
        'post.php', {
            export: ''
        }, ()=>{
            document.getElementById('export').click();
        }
    )
}

window.onload = () => {
    $.get('post.php',
        {
            all: ''
        }, (data)=>{
            data = JSON.parse(data);
            data.forEach((value)=>{
                addRow(value);
            })
        }
    )
}