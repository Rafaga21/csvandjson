<?php

date_default_timezone_set('America/Santo_Domingo');

$filename = 'People.json';
if(!file_exists($filename )){
    file_put_contents($filename , '[]');
}

$data = json_decode(file_get_contents($filename), true);
if(!empty($_POST)){
    if(isset($_POST['name']) && isset($_POST['lastname'])){
        if(!empty($_POST['name']) && !empty($_POST['lastname'])){
            $id = count($data);
            $data[$id]['id'] = $id;
            $data[$id]['name'] = $_POST['name'];
            $data[$id]['lastname'] = $_POST['lastname'];
            $data[$id]['attendance'] = true;
            $data[$id]['date'] = time();
            file_put_contents($filename, json_encode($data));
            $data[$id]['date'] = date('h:i D | M Y', $data[$id]['date']);
            echo json_encode($data[$id]);
        }
    }elseif(isset($_POST['attendance']) && isset($_POST['id'])){
        foreach($data as $index => $value){
            if($value['id'] == $_POST['id']){
                $data[$index]['attendance'] = $_POST['attendance'] == 'true';
                file_put_contents($filename, json_encode($data));
                break;
            }
        }
    }
}elseif(!empty($_GET)){
    if(isset($_GET['id'])){
        foreach($data as $value){
            if($_GET['id'] == $value['id']){
                $value['date'] = date('h:i D | M Y', $value['date']);
                echo json_encode($value);
                break;
            }
        }
    }elseif(isset($_GET['all'])){
        foreach($data as $index => $value){
            $data[$index]['date'] = date('h:i D | M Y', $data[$index]['date']);
        }
        echo json_encode($data);
    }elseif(isset($_GET['export'])){
        $text = 'People.csv';
        file_put_contents($text, '');
        $file = fopen($text, 'r+');
        foreach($data as $value){
            $value['attendance'] = $value['attendance'] ? 'Presente' : 'Ausente';
            $value['date'] = date('h:i D | M Y', $value['date']);
            foreach($value as $val){
                fwrite($file, "$val;");
            }
            fwrite($file, PHP_EOL);
        }
        fclose($file);
    }
}