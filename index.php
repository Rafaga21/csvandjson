<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba de json</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <div>
            <label for="txtname" class="text-label">Ingrese su Nombre</label><br>
            <input type="text" class="input-style" id="txtname"><br><br>

            <label for="txtlastname" class="text-label">Ingrese su Apellido</label><br>
            <input type="text" class="input-style" id="txtlastname">

            <br><br><br>
            <a href="javascript:addPeople()" class="btn">Guardar</a>
        </div>
        <div>
            <table id="people"></table>
            <br><br>
            <a href="People.csv" id="export" dounload hidden></a>
            <a href="javascript:exportPeople()" class="btn">Exportar</a>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="./script.js"></script>
</body>

</html>